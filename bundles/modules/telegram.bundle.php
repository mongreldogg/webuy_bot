<?php

namespace Bundle;

use Core\Request;
use Core\Response;

//TODO: implement group chat messages tracking and possibility to edit
//TODO: implement bot installation through console
//TODO: make tests through Telegram chat (for both types of routes - Telegram and AIO)
//TODO: implement API requests through Forehood
//TODO: test different chat IDs messaging through webhook response
//TODO: implement apiRequestJson response output
//TODO: check wait handle additional information data type, parse/compose JSON if needed

define('BOT_TYPE_TELEGRAM', 'TELEGRAM');

class Telegram extends AIOBot implements BotExtension {
	
	public static function own(){
		return parent::generateChildRoutes(new Telegram(), TELEGRAM_WEBHOOK_PATH);
	}
	
	public function __construct(){
		parent::__construct();
	}
	
	public static function getType(){
		return BOT_TYPE_TELEGRAM;
	}
	
	public static function getUserId(){
		@$userID = Request::Request()['message']['from']['id'];
		return $userID;
	}
	
	public static function getChatId(){
		@$chatID = Request::Request()['message']['chat']['id'];
		return $chatID;
	}
	
	public static function getMessageId(){
		@$msgId = Request::Request()['message']['message_id'];
		return $msgId;
	}
	
	public static function isPrivate(){
		return self::getUserId() == self::getChatId();
	}
	
	public static function getUserName(){
		@$username = Request::Request()['message']['from']['username'];
		return $username;
	}
	
	public static function DeleteMessage($msgId = null, $chatId = null){
	
		if($msgId == null) $msgId = self::getMessageId();
		if($chatId == null) $chatId = self::getChatId();
		
		$command = [
			'chat_id' => $chatId,
			'message_id' => $msgId
		];
		
		self::apiRequestJson('deleteMessage', $command);
	
	}
	
	public static function call($pattern){
		$request = Request::Request();
		switch(true){
			case isset($request['message']['new_chat_member']):
				parent::call(BOT_TYPE_TELEGRAM.'/'.ROUTE_JOIN);
				break;
			case isset($request['message']['left_chat_member']):
				parent::call(BOT_TYPE_TELEGRAM.'/'.ROUTE_LEAVE);
				break;
			default:
				$user = self::getCurrentUser('telegram_user_id',
					$request['message']['from']['id']);
				$message = null;
				if($user instanceof BotUser)
					if(Telegram::isPrivate())
						$message = $user->getWaitHandle(self::$handleData);
				if(!$message)
					@$message = $request['message']['text'];
				if(!$message) exit;
				else parent::call(BOT_TYPE_TELEGRAM.'/'.$message);
		}
	}
	
	public static function on($pattern, $callback){
		parent::$routes[BOT_TYPE_TELEGRAM.'/'.$pattern] = $callback;
	}
	
	public static function getCommand(){
		return Request::Request()['message']['text'];
	}
	
	public static function Respond(BotResponse $response, $chatID = null){
		if(!($response instanceof TelegramResponse))
			throw new \Exception('An instance of TelegramResponse required to pass into Respond, got '
				.get_class($response));
		$data = $response->getData();
		if($chatID == null) {
			$chatID = Telegram::getUserId();
			self::apiRequestWebhook($response->getMethod(), array_merge([
				'chat_id' => $chatID
			], $data));
		} elseif ($chatID != Telegram::getUserId()){
			self::apiRequestJson($response->getMethod(), array_merge([
				'chat_id' => $chatID
			], $data));
		} else {
			self::apiRequestWebhook($response->getMethod(), array_merge([
				'chat_id' => $chatID
			], $data));
		}
	}
	
	private static function apiRequestWebhook($method, $parameters) {
		if (!is_string($method)) {
			error_log("Method name must be a string\n");
			return false;
		}
		if (!$parameters) {
			$parameters = array();
		} else if (!is_array($parameters)) {
			error_log("Parameters must be an array\n");
			return false;
		}
		$parameters["method"] = $method;
		//if(__CONSOLE) var_dump($parameters);
		//else
			Response::JSON($parameters);
	}
	
	private static function apiRequestJson($method, $parameters) {
		if(__CONSOLE) return false;
		if (!is_string($method)) {
			error_log("Method name must be a string\n");
			return false;
		}
		if (!$parameters) {
			$parameters = array();
		} else if (!is_array($parameters)) {
			error_log("Parameters must be an array\n");
			return false;
		}
		$parameters["method"] = $method;
		$handle = curl_init(TELEGRAM_API_URL);
		curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($handle, CURLOPT_TIMEOUT, 60);
		curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($parameters));
		curl_setopt($handle, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		return self::exec_curl_request($handle);
	}
	
	private static function exec_curl_request($handle) {
		$response = curl_exec($handle);
		if ($response === false) {
			$errno = curl_errno($handle);
			$error = curl_error($handle);
			error_log("Curl returned error $errno: $error\n");
			curl_close($handle);
			return false;
		}
		$http_code = intval(curl_getinfo($handle, CURLINFO_HTTP_CODE));
		curl_close($handle);
		if ($http_code >= 500) {
			sleep(10);
			return false;
		} else if ($http_code != 200) {
			$response = json_decode($response, true);
			error_log("Request has failed with error {$response['error_code']}: {$response['description']}\n");
			if ($http_code == 401) {
				throw new \Exception('Invalid access token provided');
			}
			return false;
		} else {
			$response = json_decode($response, true);
			$response = $response['result'];
		}
		return $response;
	}

}

class TelegramResponse implements BotResponse {
	
	private $response = [];
	private static $buttons = [];
	
	public function __construct($responseData)
	{
		if(is_array($responseData)) $this->response = $responseData;
	}
	
	public function getData(){
		return $this->response;
	}
	
	public function Send($userID = null){
		if($userID == null) $userID = Telegram::getUserId();
		Telegram::Respond($this, $userID);
	}
	
	public static function Image($url, $caption = null){
		$response = [
			'photo' => $url,
			'caption' => $caption
		];
		if(count(self::$buttons) > 0)
			$response['reply_markup'] = [
				'keyboard' => self::$buttons,
				'one_time_keyboard' => true,
				'resize_keyboard' => true
			];
		//else $response['reply_markup'] = [
		//	'keyboard' => [],'one_time_keyboard' => true, 'resize_keyboard' => true];
		return new TelegramResponse($response);
	}
	
	public static function Buttons($buttons){
		if(is_array($buttons)) self::$buttons = $buttons;
		else self::$buttons[] = [$buttons];
	}
	
	public static function Text($text, $parseMode = null){
		$response = [
			'text' => $text
		];
		if($parseMode) $response['parse_mode'] = $parseMode;
		if(count(self::$buttons) > 0)
			$response['reply_markup'] = [
				'keyboard' => self::$buttons,
				'one_time_keyboard' => true,
				'resize_keyboard' => true
			];
		//else $response['reply_markup'] = [
		//	'keyboard' => [],'one_time_keyboard' => true, 'resize_keyboard' => true];
		return new TelegramResponse($response);
	}

	public static function HTML($html){
		return self::Text($html, "html");
	}
	
	public function getMethod(){
		switch(true){
			case isset($this->response['photo']):
				return 'sendPhoto';
				break;
			//TODO: implement any other response types
			default:
				return 'sendMessage';
				break;
		}
	}
	
}

use Bundle\BotUser;

BotUser::DefineField('telegram_user_id', TYPE_DB_NUMERIC);

AIOBot::RegisterExtension(new Telegram(), TELEGRAM_WEBHOOK_PATH);

use Core\Console;
use Core\Core;

Console::AddCommand('telegram', function($command){
	$id = null;
	$message = null;
	switch(true){
		case preg_match('/^id=[0-9]+,message=.*$/', $command):
			$command = explode(',', $command, 2);
			$id = substr($command[0], 3);
			$message = substr($command[1], 8);
			break;
		case preg_match('/^message=.*,id=[0-9]+$/', $command):
			$command = explode(',', $command, 2);
			$id = substr($command[1], 3);
			$message = substr($command[0], 8);
			break;
		default:
			echo "Wrong message! Example: id=123,message=\"Hello, World!\"";
			exit;
	}
	$_data = [];
	$_data['message']['message_id'] = (int)rand();
	$_data['message']['from']['id'] = $id;
	$_data['message']['chat']['id'] = $id;
	$_data['message']['text'] = $message;
	$_data['message']['date'] = time();
	$_REQUEST = array_merge($_REQUEST, $_data);
	$_SERVER['REQUEST_URI'] = ROOT_DIR.TELEGRAM_WEBHOOK_PATH;
	$run = new Core();
});