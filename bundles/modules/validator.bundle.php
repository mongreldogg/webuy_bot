<?php

namespace Bundle;

class Validator {
	
	public static function Domain($domain) {
		return preg_match('/^([A-Za-z0-9]|-|.)+\.[A-Za-z0-9]+$/', $domain);
	}
	
	public static function Ethereum($address) {
		return preg_match("/0x[0-9A-Fa-f]{40}/", $address);
	}

	public static function Email($email){
		return preg_match('/^([A-Za-z0-9]|-|\.)+@(([-a-z0-9]{2,100})\.)+([a-z\.]{2,8})$/', $email);
	}

	public static function CMPCO($address){
		return preg_match('/^[A-Za-z0-9]{30,64}$/', $address);
	}
	
	
}