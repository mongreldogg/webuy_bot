<?php

namespace Bundle;

use Bundle\DBObject;

class Captcha extends DBObject implements DBTableObject {
	
	public static function Generate(){
		
		$num = 5;
		$key = '';
		$chaine = "A1B2C3D4E5F6G7H8X9KMN";
		for ($i=0; $i < $num; $i++) $key .= $chaine[rand()%strlen($chaine)];
		$hash = md5(CAPTCHA_MD5_SALT.time());
		$captcha = new Captcha([
			'question' => $hash,
			'answer' => $key
		]);
		$captcha->Save();
		return $captcha;
		
	}
	
	public function getId(){
		return $this->getField('id');
	}
	
	public function getAnswer(){
		return $this->getField('answer');
	}
	
	public function setAnswer($answer){
		$this->setField('answer', $answer);
	}
	
	public function getQuestion(){
		return $this->getField('question');
	}
	
	public function setQuestion($text){
		$this->setField('question', $text);
	}
	
	private static $fields = [
		
		'id' => TYPE_DB_NUMERIC | TYPE_DB_PRIMARY,
		'question' => TYPE_DB_TEXT,
		'answer' => TYPE_DB_TEXT
		
	];
	
	public function __construct($obj = null)
	{
		parent::__construct(self::$fields, 'captcha', $obj);
	}
	
	public static function Init(){
		parent::__init('captcha', self::$fields);
	}
	
	public static function Select($rules, $count = null, $start = 0)
	{
		return parent::__select($rules, 'captcha', Captcha::class, $count, $start);
	}
	
	public static function Delete($rules)
	{
		parent::__delete($rules, 'captcha');
	}
	
	public static function Count($rules = [])
	{
		return parent::__selectCount($rules, 'captcha');
	}
	
}