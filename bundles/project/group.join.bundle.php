<?php

namespace Bundle;

use Bundle\DBObject;

class GroupJoin extends DBObject implements DBTableObject {

    public function getId(){
        return $this->getField('id');
    }

    public function getUserId(){
        return $this->getField('telegram_user_id');
    }

    public function setUserId($userId){
        $this->setField('telegram_user_id', $userId);
    }

    public function isJoined(){
        return (int)$this->getField('joined');
    }

    public static function IsGroupJoined($userId){
        $join = self::Select(['telegram_user_id'=>$userId], 1);
        if(!($join instanceof GroupJoin)) {
            return false;
        }
        elseif($join->isJoined()) return true;
        return false;
    }

    public function setJoined($joined){
        $this->setField('joined', $joined);
    }

    private static $fields = [
        'id' => TYPE_DB_NUMERIC | TYPE_DB_PRIMARY,
        'telegram_user_id' => TYPE_DB_NUMERIC,
        'joined' => TYPE_DB_NUMERIC
    ];

    public function __construct($obj = null)
	{
		parent::__construct(self::$fields, 'group_join', $obj);
	}
	
	public static function Init(){
		parent::__init('group_join', self::$fields);
	}
	
	public static function Select($rules, $count = null, $start = 0)
	{
		return parent::__select($rules, 'group_join', GroupJoin::class, $count, $start);
	}
	
	public static function Delete($rules)
	{
		parent::__delete($rules, 'group_join');
	}
	
	public static function Count($rules = [])
	{
		return parent::__selectCount($rules, 'group_join');
	}

}