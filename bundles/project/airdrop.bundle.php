<?php

namespace Bundle;

class Airdrop extends DBObject implements DBTableObject {

    public function getId(){
        return $this->getField('id');
    }

    public function getUserId(){
        return $this->getField('userid');
    }

    public function setUserId($userId){
        $this->setField('userid'. $userId);
    }

    public function getUserName(){
        return $this->getField('username');
    }

    public function setUserName($username){
        $this->setField('username', $username);
    }

    public function getEmail($email){
        return $this->getField('email');
    }

    public function setEmail($email){
        $this->setField('email', $email);
    }

    public function getTwitterHandle(){
        return $this->getField('twitter');
    }

    public function setTwitterHandle($handle){
        $this->setField('twitter', $handle);
    }

    public function getRetweeted(){
        return $this->getField('retweet');
    }

    public function setRetweeted($retweeted){
        $this->setField('retweet', (int) $retweeted);
    }

    public function getDiscord(){
        return $this->getField('discord');
    }

    public function setDiscord($instagram){
        $this->setField('discord', $instagram);
    }

    public function getFacebook(){
        return $this->getField('facebook');
    }

    public function setFacebook($facebook){
        $this->setField('facebook', $facebook);
    }

    public function getYoutube(){
        return $this->getField('youtube');
    }

    public function setYoutube($youtube){
        $this->setField('youtube', $youtube);
    }

    public function getReferrer(){
        return $this->getField('referrer');
    }

    public function setReferrer($referrer){
        $this->setField('referrer', $referrer);
    }

    public function getReferrals(){
        return $this->getField('referrals');
    }

    public function setReferrals($referrals){
        $this->setField('referrals', $referrals);
    }

    public function getTokens(){
        return $this->getField('tokens');
    }

    public function setTokens($tokens){
        $this->setField('tokens', $tokens);
    }

    public function getWallet(){
        return $this->getField('wallet');
    }

    public function setWallet($wallet){
        $this->setField('wallet', $wallet);
    }

    public function getGroupJoin(){
        return $this->getField('group_join');
    }

    public function setGroupJoin($flag){
        $this->setField('group_join', (int)$flag);
    }

    public function getDateTime(){
        return $this->getField('dt');
    }

    public function setDateTime($dt){
        $this->setField('dt', $dt);
    }

    public function Reward($tokens){
        $this->setField('tokens', (int)$this->getField('tokens') + $tokens);
    }

    private static $fields = [
        'id' => TYPE_DB_NUMERIC | TYPE_DB_PRIMARY,
        'userid' => TYPE_DB_NUMERIC, 
        'username' => TYPE_DB_TEXT,
        'email' => TYPE_DB_TEXT,
        'twitter' => TYPE_DB_TEXT,
        'discord' => TYPE_DB_TEXT,
        'facebook' => TYPE_DB_TEXT,
        'youtube' => TYPE_DB_TEXT,
        'referrer' => TYPE_DB_NUMERIC,
        'referrals' => TYPE_DB_NUMERIC,
        'tokens' => TYPE_DB_NUMERIC,
        'wallet' => TYPE_DB_TEXT,
        'group_join' => TYPE_DB_NUMERIC,
        'dt' => TYPE_DB_NUMERIC
    ];

    public function __construct($obj = null)
	{
		parent::__construct(self::$fields, 'airdrop', $obj);
	}
	
	public static function Init(){
		parent::__init('airdrop', self::$fields);
	}
	
	public static function Select($rules, $count = null, $start = 0)
	{
		return parent::__select($rules, 'airdrop', Airdrop::class, $count, $start);
	}
	
	public static function Delete($rules)
	{
		parent::__delete($rules, 'airdrop');
	}
	
	public static function Count($rules = [])
	{
		return parent::__selectCount($rules, 'airdrop');
	}

}