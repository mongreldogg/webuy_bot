Okay, great! Lets get started! 
You are now eligible to receive  {coins} coins for a few tasks. Use the tabs below to select your tasks.
 
[group_join]
Join Telegram group ({reward} coins)
[/group_join]
[twitter_join]
Join Twitter ({reward} coins)
[/twitter_join]
[discord_join]
Join Discord ({reward} coins)
[/discord_join]
[facebook_join]
Join Facebook ({reward} coins)
[/facebook_join]
[youtube_join]
Join YouTube ({reward} coins)
[/youtube_join]
[set_wallet]
Set wallet address
[/set_wallet]
[refer]
Refer a friend
[/refer]