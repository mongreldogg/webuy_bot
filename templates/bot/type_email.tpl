Hello!
I am the {project_name} bot! I can help you to subscribe to the {project_name} token giveaway.

First off, please give me your E-Mail address.

The distribution of {project_name} coins are subject to {project_name} Terms & Conditions for Token Sale and offers. 
In acquiring {project_name}, and as a condition precedent thereto, participants must acknowledge that they concent to receiving updates and offers from {project_name}. 
By continuing you agree to accept and to be bound by this agreement that token giveaway may be discontinued at anytime when maximum amount of tokens has been given away.