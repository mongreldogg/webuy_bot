__Requirements__

- OS: Any (Initially deployed testing and production instances on Debian 9)
- PHP 7 with following modules: fpm, MySQL, gd, curl
- MySQL server
- Nginx

__Installation__

- Clone a project from a current repository
- Install Nginx as a web server:

`sudo apt-get install nginx` 

- Install PHP 7.0 with all modules required:

`sudo apt-get install php7.0 php7.0-fpm php7.0-mysql php7.0-gd`

- Apply a web server configuration for sample:

`cd <project_root_directory>`

`sudo cp files/nginx-conf.conf /etc/nginx/sites-enabled/bot`

- Install MySQL:

`sudo apt-get install mysql`

- Configure project's database connection: config/core/database.config.php. Don't forget to create a user and grant all rights to a database.
Example:

`mysql`

`create user 'webuy'@'%' identified by '<db password here>';`

`create database webuy_bot;`

`grant all privileges on webuy_bot.* to 'webuy'@'%' with grant option;`

`quit`

`mysql webuy_bot < files/tbot.sql`

- Restore a database structure from file: files/tbot.sql

- Configure project's core options: config/core/config.php

- Restart a web server:

`service nginx restart`

A Telegram API requires a domain and SSL certificate for a bot to receive webHook updates.
You can use any top-level domain or if you have a subdomain with SSL certificate already assigned - also fine.

To get a free appropriate SSL certificate used by default, type the following:

`sudo certbot --authenticator standalone --installer nginx  -d <domain> --pre-hook "service nginx stop" --post-hook "service nginx start"`

Then follow steps to complete certificate installation.

__Launching a bot__

1. Open BotFather and type /newbot
2. Provide bot's name and other info required and take a token
3. Open config/telegram.config.php and change the following entry with a new token got from BotFather:

`define('TELEGRAM_BOT_TOKEN', '<bot token here>');`

4. Also set a new bot username:

`define('TELEGRAM_BOT_NAME', 'webuy_airdrop_bot');`

5. Open the following page in a browser:

`https://api.telegram.org/bot<your_bot_token>/setWebHook?url=https://<your_domain>/bot`

6. Add your bot to your Telegram group and give admin rights (for automatic users tracking)

__Usage__

---

To list all subscibers of your airdrop, open a web page:

`https://<your_domain>/download`

and type a password defined in config/bounty.config.php

`define('DASHBOARD_PASSWORD', '<password is here>');`

You can get an .xls document (spreadsheet) containing reports about users subscribed and provided their wallet addresses for tokens to receive.

---

To change rewards for actions on your Airdrop, proceed changing a configuration file config/bounty.config.php

Changing social media links is available by editing config/groups.config.php

---

Changing responses available by editing files in templates/bot folder as text files.
Changes are sensitive to line breaks.

---

