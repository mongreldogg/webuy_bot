<?php

define('CAPTCHA_ATTEMPTS', 3);
define('CAPTCHA_MD5_SALT', 'saltyson');

define('PROJECT_NAME', 'WeBuy');
define('DASHBOARD_PASSWORD', 'noWeDont$ell');
define('POLL_API_KEY', 'somekey');

define('REWARD_REFERRER', 4);

define('REWARD_TELEGRAM_JOIN', 5);
define('REWARD_TWITTER_JOIN', 3);
define('REWARD_DISCORD_JOIN', 2);
define('REWARD_FACEBOOK_JOIN', 3);
define('REWARD_YOUTUBE_JOIN', 3);
